import 'package:flutter/material.dart';
import 'package:practica_hacka_api_rest/views/basicWidgets/menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //var testing = ApiRESTConnection().apiGet('http://10.0.2.2:8000/api/waiters/');

    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Menu(),
    );
  }
}
