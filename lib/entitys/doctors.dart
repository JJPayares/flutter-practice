// ignore_for_file: empty_constructor_bodies

import 'dart:ffi';

class Doctor {
  String? name;
  String? lastName;
  String? dni;
  String? address;
  String? email;
  String? phone;
  String? gender;
  String? tuitionNumber;
  String? bornDate;
  String? registeredBy;
  String? modifiedBy;
  Bool? active;

  Doctor(
      {this.name,
      this.lastName,
      this.dni,
      this.address,
      this.email,
      this.phone,
      this.gender,
      this.tuitionNumber,
      this.bornDate,
      this.registeredBy,
      this.modifiedBy,
      this.active});

  factory Doctor.fromJson(Map<dynamic, dynamic> json) {
    return Doctor(
        name: json['name'],
        lastName: json['last_name'],
        dni: json['dni'],
        address: json['address'],
        email: json['email'],
        phone: json['phone'],
        gender: json['gender'],
        tuitionNumber: json['tuition_number'],
        bornDate: json['born_date'],
        registeredBy: json['registered_by'],
        modifiedBy: json['modified_by'],
        active: json['active']);
  }
}
