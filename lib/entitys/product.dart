class Product {
  double? price;
  String? description;
  String? image;

  Product({this.price, this.description, this.image});
}
