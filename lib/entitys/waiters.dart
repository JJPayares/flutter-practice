import 'package:flutter/material.dart';

import '../domain/connection.dart';

class Waiter {
  final int? id;
  final String? firstName;
  final String? lastName;

  Waiter({this.id, this.firstName, this.lastName});
  final url = 'http://10.0.2.2:8000/api/waiters/';

  factory Waiter.fromJson(Map<String, dynamic> json) {
    return Waiter(
      id: json["id"],
      firstName: json["first_name"],
      lastName: json["last_name"],
    );
  }

  // fetchWaiters() async {
  //   try {
  //     // var request = await ApiRESTConnection().apiGet(url);
  //     // var results = request['results'];
  //     var results = await ApiRESTConnection().getResults(url);
  //     for (var element in results) {
  //       Waiter.fromJson(element);
  //     }
  //   } catch (e) {
  //     rethrow;
  //   }
  // }
}
