import 'dart:convert';
import 'dart:developer';
import 'package:practica_hacka_api_rest/conf.dart';

import 'package:http/http.dart' as http;

class ApiRESTConnection {
  static const baseEndpoint = "http://192.168.10.11:8000/api";
  final headers = {"Authorization": "Bearer $token"};

  apiClient() {
    return http.Client();
  }

  //GET
  apiGet(endPoint) async {
    try {
      var request = await http.get(Uri.parse(endPoint));
      final response = jsonDecode(request.body);
      return response;
    } catch (e) {
      rethrow;
    }
  }

  getResults(endPoint) async {
    var request = await apiGet('$baseEndpoint/$endPoint');
    return request['results'];
  }

  Future<List> getRequest(endPoint) async {
    try {
      final request = await http.get(Uri.parse('$baseEndpoint/$endPoint'),
          headers: headers);
      final jsonResponse = jsonDecode(request.body);
      return jsonResponse['results'];
    } catch (e) {
      rethrow;
    }

    // final request = await http.get(Uri.parse(endPoint), headers: headers);
    // if (request.statusCode == 200) {
    //   final jsonResponse = jsonDecode(request.body);
    //   return jsonResponse['results'];
    // } else {
    //   throw Exception("te moriste");
    // }
  }

  Future createDoctor(
      String endPoint,
      String name,
      String lastName,
      String dni,
      String address,
      String email,
      String phone,
      String gender,
      String tuitionNumber,
      String bornDate,
      String registeredBy,
      String modifiedBy,
      String active) async {
    final body = {
      "name": name,
      "last_name": lastName,
      "dni": dni,
      "address": address,
      "email": email,
      "phone": phone,
      "gender": gender,
      "tuition_num": tuitionNumber,
      "born_date": bornDate,
      "registered_by": registeredBy,
      "modified_by": modifiedBy,
      "active": active,
    };

    try {
      final response = await http.post(Uri.parse("$baseEndpoint/$endPoint"),
          headers: headers, body: body);
      // log(body.toString());
      return response;
    } catch (e) {
      // log(body.toString());
      rethrow;
    }

    // final response = await http.post(Uri.parse("$baseEndpoint/$endPoint"),
    //     headers: headers, body: body);

    // if (response.statusCode == 201) {
    //   log("USUARIO $name creado");
    //   return response;
    // } else {
    //   throw Exception("Hubo un error");
    // }
  }
}
