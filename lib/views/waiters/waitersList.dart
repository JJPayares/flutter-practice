import 'package:flutter/material.dart';
import 'package:practica_hacka_api_rest/domain/connection.dart';

class WaitersList extends StatefulWidget {
  const WaitersList({Key? key}) : super(key: key);

  @override
  State<WaitersList> createState() => _WaitersListState();
}

class _WaitersListState extends State<WaitersList> {
  var fetchedWaiters = [];

  @override
  Widget build(BuildContext context) {
    fetchWaiters();

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de meseros'),
      ),
      body: ListView.builder(
          itemCount: fetchedWaiters.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(
                  '${fetchedWaiters[index]["first_name"]} ${fetchedWaiters[index]["last_name"]}'),
            );
          }),
    );
  }

  fetchWaiters() async {
    try {
      var getResults = await ApiRESTConnection().getResults('waiters');
      if (fetchedWaiters.isEmpty) {
        setState(() {
          fetchedWaiters = getResults;
        });
      }
    } catch (e) {
      rethrow;
    }
  }
}
