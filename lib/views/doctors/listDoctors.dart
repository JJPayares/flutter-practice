import 'package:flutter/material.dart';
import 'package:practica_hacka_api_rest/domain/connection.dart';
import 'package:practica_hacka_api_rest/views/basicWidgets/baseCard.dart';

class ListDoctors extends StatelessWidget {
  final getDoctors = ApiRESTConnection().getRequest('doctors');

  @override
  Widget build(BuildContext context) {
    getDoctors.then((value) => print(value));
    return Scaffold(
        body: FutureBuilder<List>(
      future: getDoctors,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        } else if (snapshot.hasError) {
          return const Center(child: Text("Hubo un error"));
        } else {
          return ListView.builder(
              itemCount: snapshot.data?.length,
              itemBuilder: (context, index) {
                final currentDoctor = snapshot.data?[index];

                return BaseCard(element: currentDoctor);
              });
        }
      },
    ));
  }
}
