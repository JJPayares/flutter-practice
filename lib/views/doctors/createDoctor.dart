import 'package:flutter/material.dart';
import 'package:practica_hacka_api_rest/domain/connection.dart';
import 'package:practica_hacka_api_rest/views/doctors/listDoctors.dart';

class CreateDoctor extends StatelessWidget {
  final TextEditingController _name = TextEditingController();
  final TextEditingController _lastName = TextEditingController();
  final TextEditingController _dni = TextEditingController();
  final TextEditingController _address = TextEditingController();
  final TextEditingController _email = TextEditingController();
  final TextEditingController _phone = TextEditingController();
  final TextEditingController _gender = TextEditingController();
  final TextEditingController _tuitionNumber = TextEditingController();
  final TextEditingController _bornDate = TextEditingController();
  final TextEditingController _registeredBy = TextEditingController();
  final TextEditingController _modifiedBy = TextEditingController();
  final TextEditingController _active = TextEditingController();

  final ApiRESTConnection client = ApiRESTConnection();
  CreateDoctor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Scaffold(
        appBar: AppBar(
          title: const Text('Crear un doctor'),
        ),
        body: Center(
          child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: ListView(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Name"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _name,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Last Name"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _lastName,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("DNI"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _dni,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Address"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _address,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Email"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _email,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Phone"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _phone,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Gender"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _gender,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Tuition Number"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _tuitionNumber,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Born Date"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _bornDate,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Registered By"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _registeredBy,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Modified By"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _modifiedBy,
                      ),
                      const SizedBox(height: 10),
                      TextField(
                        decoration: const InputDecoration(
                            label: Text("Active"),
                            hintText: "Aqui coloca el nombre"),
                        controller: _active,
                      ),
                      const SizedBox(height: 10),
                      ElevatedButton(
                          onPressed: () {
                            client.createDoctor(
                              'doctors/',
                              _name.text,
                              _lastName.text,
                              _dni.text,
                              _address.text,
                              _email.text,
                              _phone.text,
                              _gender.text,
                              _tuitionNumber.text,
                              _bornDate.text,
                              _registeredBy.text,
                              _modifiedBy.text,
                              _active.text,
                            );
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (_) => ListDoctors()));
                          },
                          child: const Text("Registrar")),
                    ],
                  ),
                ],
              )),
        ),
      ),
    );
  }
}
