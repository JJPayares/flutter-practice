import 'package:flutter/material.dart';

class BaseCard extends StatelessWidget {
  final Map<dynamic, dynamic> element;

  BaseCard({Key? key, required this.element}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        clipBehavior: Clip.antiAlias,
        child: Row(
          children: [
            Column(
              children: const [
                SizedBox(
                  height: 10,
                ),
                CircleAvatar(
                  backgroundColor: Color.fromARGB(255, 139, 215, 250),
                  minRadius: 40.0,
                  child: CircleAvatar(
                    radius: 37.0,
                    backgroundImage:
                        AssetImage('assets/images/defaultDoctorProfPic.jpg'),
                  ),
                ),
                SizedBox(
                  height: 10,
                )
              ],
            ),
            Column(
              children: const [
                SizedBox(
                  width: 20,
                ),
              ],
            ),
            Flexible(
                child: Column(
              children: [
                ListTile(
                  trailing: IconButton(
                    icon: const Icon(
                      Icons.navigate_next,
                      size: 20,
                      color: Colors.green,
                    ),
                    onPressed: () {
                      // Navigator.push(
                      //     context,
                      //     MaterialPageRoute(
                      //         builder: (context) => ProfileView(
                      //               registry: registry,
                      //             )));
                      print(element);
                    },
                  ),
                  title: Text('${element['name']} ${element['last_name']}'),
                  subtitle: Text(
                    '${element['email']}',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
              ],
            ))
          ],
        ),
      ),
    );
  }
}
